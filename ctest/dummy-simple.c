#define _GNU_SOURCE
#include <unistd.h>
#include <stdlib.h>
#include <sys/syscall.h>

int main (int argc, char **argv)
{
	if (argc < 2)
		return 0;
	return syscall(333,argv[1], atoi(argv[2])) < 0; 
}

